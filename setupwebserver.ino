void setupwebserver()
{
  //----------------------
   // Initialize SPIFFS
  if(!SPIFFS.begin()){
    Serial.println("An Error has occurred while mounting SPIFFS");
    return;
  }


  //-------------------------
  // Route for root / web page
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(SPIFFS, "/index.html");
  });
  server.on("/temperature", HTTP_GET, [](AsyncWebServerRequest *request){
    //request->send_P(200, "text/plain", readHTU21Temperature().c_str()); //send webpage
    request->send(200, "text/plain", readHTU21Temperature().c_str());
    
  });
  server.on("/humidity", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send_P(200, "text/plain", readHTU21Humidity().c_str());
  });
  

  // Start server
  server.begin();
}

String readHTU21Temperature() {
  float t=myHumidity.readTemperature();
  return String(t);
  }
  
String readHTU21Humidity() {
  float h=myHumidity.readHumidity();;
  return String(h);
  }
 
