//various libraries etc
#define fDebug true
byte heartbeat=0;
//---------------------
#define HOSTNAME "HTU21b"
#define PUYA_SUPPORT
#define PUYASUPPORT
const char*             MQTT_VENT_COMMAND_TOPIC  = "home/HTU21/switch";
#define MQTT_VERSION    MQTT_VERSION_3_1_1
const PROGMEM uint16_t  MQTT_SERVER_PORT          = 1883;
const PROGMEM char*     MQTT_CLIENT_ID            = HOSTNAME;//"HTU21";
const PROGMEM char*     MQTT_USER                 = "";
const PROGMEM char*     MQTT_PASSWORD             = "";
char mqtt_server[40];
char data[80];
long starttime;         // determines the update period
long currenttime;       // ditto
String connectedSSID;

#define SDA 0     //op Wemos GPIO5 D1
#define SCL 2     //opWemos GPIO4 D2

///tijd
#include <SNTPtime.h>
SNTPtime NTPch("nl.pool.ntp.org");
strDateTime dateTime;
float HTUhumd;
float  HTUtemp;

/*
 *consider 
char HostName[32];
sprintf(HostName, "IJskast-%06X", ESP.getChipId());
wifi_station_set_hostname(HostName);
 * /
 */

byte actualHour;
byte actualMinute;
byte actualsecond;
int actualyear;
byte actualMonth;
byte actualday;
byte actualdayofWeek;
int doY; //to be calculated
